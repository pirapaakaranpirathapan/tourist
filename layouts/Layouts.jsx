import * as React from 'react';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import styles from '../layouts/layouts.module.css'

import Navbar from '../components/Navbar/Navbar';

import Footer from '../components/Footer/Footer'

import Sidebar from '../components/Sidebar/Sidebar';

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));

export default function BasicGrid({children}) {
  return (
    <Box sx={{ flexGrow: 1 }} className={styles.boxlayout} >
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Item>
            <Navbar />
          </Item>
        </Grid>
        <Grid item xs={2}>
          <Item> <Sidebar /> </Item>
        </Grid>
        
        <Grid item xs={10}>
          <Item>
          {children}
          </Item>
        </Grid>
        <Grid item xs={12}>
          <Item> <Footer /> </Item>
        </Grid>
      </Grid>
    </Box>
  );
}
