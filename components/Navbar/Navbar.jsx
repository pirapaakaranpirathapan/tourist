import * as React from 'react';
import {AppBar,Box,Typography,IconButton,Toolbar,Button,} from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import styles from './Navbar.module.css'
import Link from 'next/link';



export default function ButtonAppBar() {

  const menubutton = ()=>{
    alert("Comming soon")
  }
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static" className={styles.navbar} >
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
            onClick={()=>menubutton()}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            "name"
          </Typography>

          <Link href="/srilankannews">
           <Button color="inherit">Srilankan</Button>
          </Link>

         
        </Toolbar>
      </AppBar>
    </Box>
  );
}
