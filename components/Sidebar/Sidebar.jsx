import React from 'react'
import styles from './Sidebar.module.css'
import { Button } from '@mui/material';
import Link from 'next/link';

function Sidebar() {
  return (
    <div className={styles.sidebar}>
        <div className={styles.menu}>
        <Link href="/tourist">
        <Button variant="contained" color="success" className={styles.button}>சுற்றுலாத்தளங்கள்   </Button>
      </Link>

      <Link href="/srilankannews">
        <Button variant="contained" color="success" className={styles.button}>மருத்துவம்   </Button>
      </Link>

      <Link href="/srilankannews">
        <Button variant="contained" color="success" className={styles.button}>தொழில்நுட்பம்   </Button>
      </Link>

      <Link href="/srilankannews">
        <Button variant="contained" color="success" className={styles.button}>அறிவியல்   </Button>
      </Link>

      <Link href="/srilankannews">
        <Button variant="contained" color="success" className={styles.button}>மர்மங்கள்   </Button>
      </Link>

      <Link href="/srilankannews">
        <Button variant="contained" color="success" className={styles.button}>சமையல்   </Button>
      </Link>
    



      </div>



    </div>
  )
}

export default Sidebar