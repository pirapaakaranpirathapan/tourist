import React from 'react'

import { Card } from 'react-bootstrap';
import Button from '@mui/material/Button';
import styles from './box.module.css'

function PostBoot(props) {
    return (
        <>
            <Card style={{ width: '18rem' }} className={styles.box}>
                <Card.Img variant="top" src="holder.js/100px180" />
                <Card.Body>
                    <Card.Title> {props.title} </Card.Title>
                    <Card.Text>
                        {props.text}
                    </Card.Text>

                    <Button variant="contained" color="success">
                        மேலும் படிக்க... 
                    </Button>
                </Card.Body>
            </Card>


        </>
    )
}

export default PostBoot