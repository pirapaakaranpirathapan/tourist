import React from 'react'
import Contentbox from '../../components/elements/contentbox'
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';

const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  }));
  
  export default function BasicGrid({children}) {
    return (
      <Box sx={{ flexGrow: 1 }}  >
        <Grid container spacing={2}>
          <Grid item xs={4}>
            <Item>
            <Contentbox title="heading" text="body" />
            </Item>
          </Grid>
          <Grid item xs={4}>
            <Item>  <Contentbox title="heading" text="body" /> </Item>
          </Grid>
          
          <Grid item xs={4}>
            <Item>
            <Contentbox title="heading" text="body" />
            </Item>
          </Grid>
          <Grid item xs={4}>
            <Item>  <Contentbox title="heading" text="body" /></Item>
          </Grid>
        </Grid>
      </Box>
    );
  }